#!/usr/bin/env bash

#######################################################################
## Tools
#######################################################################
apt-get install git htop atop vim p7zip-full mc -y

#######################################################################
## MySQL
#######################################################################
wget https://repo.percona.com/apt/percona-release_latest.$(lsb_release -sc)_all.deb
dpkg -i percona-release_latest.$(lsb_release -sc)_all.deb
percona-release setup ps80
apt-get install percona-server-server -y

mysql -p -e "CREATE FUNCTION fnv1a_64 RETURNS INTEGER SONAME 'libfnv1a_udf.so'"
mysql -p -e "CREATE FUNCTION fnv_64 RETURNS INTEGER SONAME 'libfnv_udf.so'"
mysql -p -e "CREATE FUNCTION murmur_hash RETURNS INTEGER SONAME 'libmurmur_udf.so'"

#######################################################################
## PHP
#######################################################################
apt-get update
apt install gnupg2 -y
wget -qO - https://packages.sury.org/php/apt.gpg | sudo apt-key add -
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php7.x.list
apt-get update
apt upgrade

apt-get install php7.4 php7.4-cli php7.4-common php7.4-curl php7.4-gd php7.4-json php7.4-mysql php7.4-opcache php7.4-pgsql php7.4-xml \
php7.4-bz2 php7.4-fpm php7.4-intl php7.4-mbstring php7.4-zip php7.4-bcmath php7.4-dev php-pear redis-server curl -y

pecl install igbinary
echo "extension=igbinary.so" > /etc/php/7.4/mods-available/igbinary.ini
ln -s /etc/php/7.4/mods-available/igbinary.ini /etc/php/7.4/cli/conf.d/igbinary.ini
ln -s /etc/php/7.4/mods-available/igbinary.ini /etc/php/7.4/fpm/conf.d/igbinary.ini

pecl install redis
echo "extension=redis.so" > /etc/php/7.4/mods-available/redis.ini
ln -s /etc/php/7.4/mods-available/redis.ini /etc/php/7.4/cli/conf.d/redis.ini
ln -s /etc/php/7.4/mods-available/redis.ini /etc/php/7.4/fpm/conf.d/redis.ini

#######################################################################
## Openresty
#######################################################################
wget -qO - https://openresty.org/package/pubkey.gpg | sudo apt-key add -
apt-get -y install software-properties-common
add-apt-repository -y "deb http://openresty.org/package/ubuntu $(lsb_release -sc) main"
apt-get update
apt-get install openresty
ln -s /usr/local/openresty/nginx/conf/ /etc/nginx
mkdir /var/log/nginx/


#######################################################################
## Composer
#######################################################################
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === 'a5c698ffe4b8e849a443b120cd5ba38043260d5c4023dbf93e1558871f1f07f58274fc6f4c93bcfd858c6bd0775cd8d1') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php --install-dir=/usr/bin --filename=composer
php -r "unlink('composer-setup.php');"