#!/bin/bash

echo "vm.swappiness=10" >> /etc/sysctl.conf
sysctl -p

apt update
apt dist-upgrade

my_dir="$(dirname "$0")"

wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
apt-get update
apt-get install google-chrome-stable -y

wget -q -O - https://repo.skype.com/data/SKYPE-GPG-KEY | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] https://repo.skype.com/deb stable main" > /etc/apt/sources.list.d/skype-stable.list'
apt-get update && apt-get install skypeforlinux -y

# https://losst.ru/ustanovka-telegram-ubuntu-16-04
add-apt-repository ppa:atareao/telegram
apt update
apt install telegram

"$my_dir/server.sh"

# сборка redis-manager
# http://docs.redisdesktop.com/en/latest/install/
# https://github.com/uglide/RedisDesktopManager/issues/3761
apt-get install libqt5concurrent5 libqt5concurrent5:i386 zlibc libicu-dev -y
source /opt/qt59/bin/qt59-env.sh && qmake && make && make install
wget https://github.com/uglide/RedisDesktopManager/releases/download/0.9.0-alpha4/redis-desktop-manager_0.9.0.17_amd64.deb
dpkg -i redis-desktop-manager_0.9.0.17_amd64.deb
rm -f redis-desktop-manager_0.9.0.17_amd64.deb


cd ~
apt autoremove
updatedb

